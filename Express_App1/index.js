import express, { json } from "express"
import firstRouter from "./src/Router/firstRouter.js";
import bikeRouter from "./src/Router/bikeRouter.js";
import traineeRouter from "./src/Router/traineeRouter.js";
import vehiclesRouter from "./src/Router/vehiclesRouter.js";
let expressApp=express();
expressApp.use(json()); // For support json Data==>Always place this code at top of the router

let port=8000;
expressApp.listen(port,()=>{
    console.log(`Express app is Listening at port ${port}`)
})


//import firstRouter
expressApp.use("/",firstRouter); //localhost:8000

// import bikeRouter
expressApp.use("/bike",bikeRouter);

//import traineeRouter
expressApp.use("/trainee",traineeRouter);


//import vehiclesRouter
expressApp.use("/vehicles",vehiclesRouter)