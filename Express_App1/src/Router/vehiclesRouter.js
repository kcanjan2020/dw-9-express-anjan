import { Router } from "express";

let vehiclesRouter=Router();
vehiclesRouter.route("/")
.post(()=>{
    console.log({success:true,message:"Vehicles created successfully"})
})
.get(()=>{
    console.log({success:true,message:"Vehicles read successfully"})
})
.patch(()=>{
    console.log({success:true, message:"Vehicles updated successfully"})
})
.delete(()=>{
    console.log({success:true,message:"Vehicles Deleted successfully"})
})

export default vehiclesRouter;