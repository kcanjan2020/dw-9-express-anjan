import { Router } from "express";

let firstRouter=Router();
firstRouter.route("/")  //localhost:8000==>While selecting api it sees route it doest sees url
.post(()=>{
    console.log("Home Post");
})
.get(()=>{
    console.log("Home Get");
})
.patch(()=>{
    console.log("Home Patch");
})
.delete(()=>{
    console.log("Home Delete")
});

firstRouter.route("/name") // localhost:8000/name
.post(()=>{
    console.log("Name Post")
})


export default firstRouter;