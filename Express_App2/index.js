import express, { json } from "express"
import firstRouter from "./firstRouter.js";

let expressApp=express();
expressApp.use(json());
let port=8000;

expressApp.listen(port,()=>{
    console.log(`Express App is Listening at port ${port}`)
})


//import firstRouter
expressApp.use("/",firstRouter)